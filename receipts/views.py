from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.


@login_required
def receipt_listview(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_listview": receipts
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/accounts/create.html", context)


@login_required
def categories_listview(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_listview": categories
    }
    return render(request, "receipts/categories.html", context)


@login_required
def accounts_listview(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts_listview": accounts
    }
    return render(request, "receipts/accounts.html", context)
