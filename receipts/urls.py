from django.urls import path
from receipts.views import receipt_listview, create_receipt, categories_listview, accounts_listview, create_category, create_account


urlpatterns = [
    path("", receipt_listview, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", categories_listview, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", accounts_listview, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
